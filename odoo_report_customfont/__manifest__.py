# -*- coding: utf-8 -*-
{
    'name': 'Odoo Custom Font',
    'version': '11.0.1.0.0',
    'author': 'Portcities Ltd',
    'website': 'http://www.portcities.net',
    'summary': "Monkey Patch",
    'contributors': [
        'Dhimas Yudangga A',
    ],
    'description': """
    Custom Font
    """,
    'sequence': 1,
    'depends': [
        'sale',
    ],
    'data': [
        'report/sale_report_templates.xml',
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}
