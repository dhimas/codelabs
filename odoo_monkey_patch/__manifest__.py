# -*- coding: utf-8 -*-
{
    'name': 'Odoo Monkey Patch',
    'version': '11.0.1.0.0',
    'author': 'Portcities Ltd',
    'website': 'http://www.portcities.net',
    'summary': "Monkey Patch",
    'contributors': [
        'Dhimas Yudangga A',
    ],
    'description': """
    Monkeypatch function
    """,
    'sequence': 1,
    'depends': [
        'base',
    ],
    'post_load': 'new_float_round',
    'pre_init_hook': None,
    'post_init_hook': None,
    'auto_install': False,
    'installable': True,
    'application': True,
}
